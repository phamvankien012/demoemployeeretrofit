package com.hlub.dev.demomvp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.hlub.dev.demomvp.retrofit.EmployeeRetrofit;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateActivity extends AppCompatActivity {
    private EditText edtName;
    private EditText edtAge;
    private EditText edtSalary;
    private Button btnCreate;
    private Toolbar toolbarCreate;
    private ImageView imgBack;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        toolbarCreate = findViewById(R.id.toolbar_create);
        edtName = (EditText) findViewById(R.id.edtName);
        edtAge = (EditText) findViewById(R.id.edtAge);
        edtSalary = (EditText) findViewById(R.id.edtSalary);
        btnCreate = (Button) findViewById(R.id.btnCreate);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgerssDialogCreate();

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void createEmployee() {

        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", edtName.getText().toString());
        jsonObject.addProperty("salary", edtSalary.getText().toString());
        jsonObject.addProperty("age", edtAge.getText().toString());

        if (edtName.getText().toString().isEmpty() || edtAge.getText().toString().isEmpty() || edtSalary.getText().toString().isEmpty()) {
            Toast.makeText(this, "Phải nhập  nhân viên", Toast.LENGTH_SHORT).show();
        }
        Call<ResponseBody> createEmp = EmployeeRetrofit.getInstance().createEmployee(jsonObject);
        createEmp.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(CreateActivity.this, "Thêm nhân viên thất bại", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CreateActivity.this, "Thêm nhân viên thành công", Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();
                setViewEmpity();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CreateActivity.this, "Thêm nhân viên thất bại", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                setViewEmpity();
                Log.e("create", t.toString());
            }
        });
    }

    private void setViewEmpity() {
        edtSalary.setText("");
        edtName.setText("");
        edtAge.setText("");
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
        super.onBackPressed();
    }

    private void showProgerssDialogCreate() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        //show progessdialog + create employee
        createEmployee();


    }


}
