package com.hlub.dev.demomvp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hlub.dev.demomvp.adapter.EmployeeAdapter;
import com.hlub.dev.demomvp.entity.Employee;
import com.hlub.dev.demomvp.receiver.NetworkChangeReceiver;
import com.hlub.dev.demomvp.retrofit.EmployeeRetrofit;
import com.hlub.dev.demomvp.retrofit.EmployeeService;

import java.util.ArrayList;
import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView recycleviewEmployee;
    private FloatingActionButton flAddEmployee;
    private TextView tvInternet;
    private SwipeRefreshLayout swipeContainer;
    private ProgressBar progessbar;


    List<Employee> employeeList;
    EmployeeAdapter employeeAdapter;
    LinearLayoutManager linearLayoutManager;
    private IntentFilter filter;

    private BroadcastReceiver reciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isNetworkAvailable();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recycleviewEmployee = findViewById(R.id.recycleviewEmployee);
        flAddEmployee = findViewById(R.id.flAddEmployee);
        tvInternet = findViewById(R.id.tvInternet);
        swipeContainer = findViewById(R.id.swipe_container);
        progessbar = findViewById(R.id.progessbar);


        flAddEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CreateActivity.class));
            }
        });

        employeeList = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(this);
        employeeAdapter = new EmployeeAdapter(this, employeeList);
        recycleviewEmployee.setAdapter(employeeAdapter);
        recycleviewEmployee.setLayoutManager(linearLayoutManager);

        filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(reciever, filter);

        swipeContainer.setOnRefreshListener(this);

        swipeContainer.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (registerReceiver(reciever, filter) != null) {
            unregisterReceiver(reciever);
        }
    }

    private void requestDataFromServer() {

        swipeContainer.setRefreshing(true);

        EmployeeRetrofit.getInstance().getEmployees().enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                swipeContainer.setRefreshing(false);

                employeeList.addAll(response.body());
                employeeAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {
                swipeContainer.setRefreshing(false);
            }
        });
    }


    private void isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {

            requestDataFromServer();

            recycleviewEmployee.setVisibility(View.VISIBLE);
            flAddEmployee.setVisibility(View.VISIBLE);

        } else {
            recycleviewEmployee.setVisibility(View.INVISIBLE);
            flAddEmployee.setVisibility(View.INVISIBLE);
            tvInternet.setVisibility(View.VISIBLE);

        }
    }


    public void showDialogDeleteEmployee(final int id, final int pos) {
        final Dialog dialog = new Dialog(this);

        final View view = LayoutInflater.from(this).inflate(R.layout.dialog_delete, null);

        TextView tvDeleteHuy = view.findViewById(R.id.tvDeleteHuy);
        TextView tvDeleteOK = view.findViewById(R.id.tvDeleteOK);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tvDeleteHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvDeleteOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<Employee> deleteEmp = EmployeeRetrofit.getInstance().deleteEmployee(id);
                deleteEmp.enqueue(new Callback<Employee>() {
                    @Override
                    public void onResponse(Call<Employee> call, Response<Employee> response) {

                        dialog.dismiss();
                        showProgerssDialog(pos);
                        Toast.makeText(MainActivity.this, "Đã xóa Employee", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Employee> call, Throwable t) {
                        dialog.dismiss();
                        showProgerssDialog(pos);
                        Toast.makeText(MainActivity.this, "Xóa Employee không thành công", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        dialog.setContentView(view);
        dialog.show();

    }


    @Override
    public void onRefresh() {
        requestDataFromServer();
    }

    private void showProgerssDialog(final int pos) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        employeeList.remove(pos);
                        employeeAdapter.notifyDataSetChanged();
                    }
                });

            }
        }).start();
        progressDialog.show();
    }
}
