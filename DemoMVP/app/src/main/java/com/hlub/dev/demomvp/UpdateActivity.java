package com.hlub.dev.demomvp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blackcat.currencyedittext.CurrencyEditText;
import com.google.gson.JsonObject;
import com.hlub.dev.demomvp.entity.Employee;
import com.hlub.dev.demomvp.retrofit.EmployeeRetrofit;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import javax.xml.transform.Result;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity {

    private EditText edtName;
    private EditText edtAge;
    private CurrencyEditText edtSalary;
    private Button btnUpdate;
    private ImageView imgBack;
    private ProgressDialog progressDialog;

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        edtName = (EditText) findViewById(R.id.edtName);
        edtAge = (EditText) findViewById(R.id.edtAge);
        edtSalary = (CurrencyEditText) findViewById(R.id.edtSalary);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        Intent intent = getIntent();
        bundle = intent.getBundleExtra("bundle");

        getSingleEmployee();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgerssDialogUpdate();
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getSingleEmployee() {

        edtName.setText(bundle.getString("name"));
        edtAge.setText(bundle.getString("age"));
        edtSalary.setText(bundle.getString("salary"));

    }

    private void updateEmployee(final int id) {
        final JsonObject jsonObject = new JsonObject();

        String money = edtSalary.getText().toString().replace(".", "");
        final String moneyNumber = money.substring(2).trim();
        Log.e("udpate", moneyNumber);


        jsonObject.addProperty("name", edtName.getText().toString());
        jsonObject.addProperty("salary", moneyNumber);
        jsonObject.addProperty("age", edtAge.getText().toString());
        if (edtName.getText().toString().isEmpty() || edtAge.getText().toString().isEmpty() || edtSalary.getText().toString().isEmpty()) {
            Toast.makeText(this, "Phải nhập tên nhân viên", Toast.LENGTH_SHORT).show();
        }
        Call<ResponseBody> createEmp = EmployeeRetrofit.getInstance().updateEmployee(id, jsonObject);
        createEmp.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(UpdateActivity.this, "Update nhân viên thất bại", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(UpdateActivity.this, "Update nhân viên thành công", Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();
                startActivity(new Intent(UpdateActivity.this, MainActivity.class));
                finish();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(UpdateActivity.this, "Update nhân viên thất bại", Toast.LENGTH_SHORT).show();
                Log.e("update", t.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
        super.onBackPressed();
    }

    private void showProgerssDialogUpdate() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        updateEmployee(Integer.parseInt(bundle.getString("id")));


    }
}
