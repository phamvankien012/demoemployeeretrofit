package com.hlub.dev.demomvp.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hlub.dev.demomvp.MainActivity;
import com.hlub.dev.demomvp.R;
import com.hlub.dev.demomvp.UpdateActivity;
import com.hlub.dev.demomvp.entity.Employee;
import com.hlub.dev.demomvp.retrofit.EmployeeRetrofit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    MainActivity context;
    List<Employee> employeeList;

    private final int ROW_1 = 1;
    private final int ROW_2 = 2;
    private final int ROW_3 = 3;
    private final int ROW_LOADING = 0;


    public EmployeeAdapter(MainActivity context, List<Employee> employeeList) {
        this.context = context;
        this.employeeList = employeeList;
    }


    @Override
    public int getItemViewType(int position) {
        int pos = position + 1;

        if (employeeList.get(position) == null) {
            return ROW_LOADING;
        } else {
            if (pos % 5 == 0) {
                return ROW_1;
            } else if (pos % 3 == 0) {
                return ROW_2;
            } else if (pos % 2 == 0) {
                return ROW_3;
            } else {
                return ROW_1;
            }
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        switch (viewType) {
            case ROW_1:
                return new EmployeeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_employee_a, parent, false));
            case ROW_2:
                return new EmployeeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_employee_b, parent, false));
            case ROW_3:
                return new EmployeeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_employee_c, parent, false));
            case ROW_LOADING:
                return new ViewHolderLoading(LayoutInflater.from(context).inflate(R.layout.item_loading, parent, false));
            default:
                return null;
        }


    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof EmployeeViewHolder) {
            Random rand = new Random();
            //Lương nhân ramdom từ 50 -> 200 sau đó format thành 1.000.000 vnđ
            final double salary = Double.parseDouble(employeeList.get(position).getEmployeeSalary()) * (rand.nextInt(151) + 50);
            final String name = employeeList.get(position).getEmployeeName().toString();
            final String age = employeeList.get(position).getEmployeeAge().toString();
            final int id = Integer.parseInt(employeeList.get(position).getId().toString());

            EmployeeViewHolder employeeViewHolder = (EmployeeViewHolder) holder;
            employeeViewHolder.tvItemName.setText(name);
            employeeViewHolder.tvItemMoney.setText(formatVnCurrence(salary) + " VNĐ");
            employeeViewHolder.tvItemAge.setText(age);
            employeeViewHolder.imgItemDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.showDialogDeleteEmployee(id, position);
                }
            });

            employeeViewHolder.containerUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, UpdateActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", String.valueOf(id));
                    bundle.putString("name", name);
                    bundle.putString("age", age);
                    bundle.putString("salary", formatVnCurrence(salary));
                    intent.putExtra("bundle", bundle);
                    context.startActivity(intent);
                }
            });
        }else if(holder instanceof ViewHolderLoading){
            ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }


    }


    @Override
    public int getItemCount() {
        return employeeList==null?0:employeeList.size();
    }

    public class EmployeeViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItemName;
        private TextView tvItemAge;
        private TextView tvItemMoney;
        private ImageView imgItemDelete;
        private LinearLayout containerUpdate;

        public EmployeeViewHolder(View itemView) {
            super(itemView);

            tvItemName = (TextView) itemView.findViewById(R.id.tvItemName);
            tvItemAge = (TextView) itemView.findViewById(R.id.tvItemAge);
            tvItemMoney = (TextView) itemView.findViewById(R.id.tvItemMoney);
            imgItemDelete = (ImageView) itemView.findViewById(R.id.imgItemDelete);
            containerUpdate = (LinearLayout) itemView.findViewById(R.id.container_update);

        }
    }


    private class ViewHolderLoading extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ViewHolderLoading(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.itemProgressbar);
        }
    }


    public static String formatVnCurrence(double price) {
        DecimalFormat formatter = new DecimalFormat("###,###,###.##");
        String str1 = formatter.format(Math.floor(price));

        return String.valueOf(str1);
    }


}
